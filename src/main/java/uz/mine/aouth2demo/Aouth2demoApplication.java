package uz.mine.aouth2demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Aouth2demoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Aouth2demoApplication.class, args);
    }

}
